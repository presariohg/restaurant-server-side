from    PyQt5.uic           import  loadUi
from    PyQt5.QtWidgets     import  QMainWindow, QTableWidgetItem, QApplication, QDialog
from    PyQt5.QtCore        import  Qt

from global_variables   import  *
import random
import sys

class View(QDialog):
    def __init__(self):
        super(View, self).__init__()
        loadUi('checkout.ui', self)

        self.setFixedSize(self.size())

        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_DISH, 250)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_AMOUNT, 100)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_PRICE, 130)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_TOTAL, 200)

        # self.COLOR_DEFAULT      =   self.lw_table_list.item(0).background()

        self.buttonBox.accepted.connect(self.foo)
        self.show_selected_table()

    def show_selected_table(self):
        self.tw_order_list.setRowCount(0)

        sum = 0

        for i in (range(4)):
            row_current     =   self.tw_order_list.rowCount()

            dish            =   QTableWidgetItem(str(i))
            amount          =   QTableWidgetItem(str(random.randint(1, 10)))
            price           =   QTableWidgetItem(str(random.randint(50, 200)))
            total           =   QTableWidgetItem(str(int(price.text()) * int(amount.text())))
            sum             +=  int(total.text())

            self.tw_order_list.insertRow(row_current)

            self.tw_order_list.setItem(row_current, COLUMN_DISH, dish)
            self.tw_order_list.setItem(row_current, COLUMN_AMOUNT, amount)
            self.tw_order_list.setItem(row_current, COLUMN_PRICE, price)
            self.tw_order_list.setItem(row_current, COLUMN_TOTAL, total)

        self.lb_sum.setText(f"Total: {sum}")

    def foo(self):
        print('accepted')

app         =   QApplication(sys.argv)
view  =   View()
view.show()

sys.exit(app.exec_())