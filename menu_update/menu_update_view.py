from    PyQt5.uic           import  loadUi
from    PyQt5.QtWidgets     import  QDialog, QHeaderView, QTreeWidgetItem, QSpinBox, QLineEdit
from    PyQt5.QtCore        import  Qt, QItemSelectionModel

COLUMN_ITEM_NAME    =   0
COLUMN_ID           =   1
COLUMN_LEFT         =   2
COLUMN_PRICE        =   3

class View(QDialog):
    def __init__(self):
        super(View, self).__init__()
        loadUi('./UI/menu_update.ui', self)

        self.setFixedSize(self.size())
        self.trw_menu_table.header().setSectionResizeMode(QHeaderView.Fixed)
        self.trw_menu_table.header().setSectionsMovable(False)
        # set fixed column size
        self.trw_menu_table.header().resizeSection(COLUMN_ITEM_NAME, 300)
        self.trw_menu_table.header().resizeSection(COLUMN_ID, 50)
        self.trw_menu_table.header().resizeSection(COLUMN_LEFT, 130)
        self.trw_menu_table.header().resizeSection(COLUMN_PRICE, 200)

    def add_item(self, item_id, this_item):
        wi_this_item    =   QTreeWidgetItem()
        parent_id       =   this_item['parent']

        # has no parent, so it must be at top level
        if (parent_id   ==  None):
            self.trw_menu_table.addTopLevelItem(wi_this_item)
        else:
            # it has a parent, so let's find this parent
            try:
                wi_parent   =   self.trw_menu_table.findItems(parent_id, Qt.MatchRecursive, COLUMN_ID)[0]
            except IndexError:
                import sys
                print('faulty json menu data')
                sys.exit()

            wi_parent.addChild(wi_this_item)

        # if it's a leaf, add leaves' exclusive properties
        if (this_item['is_leaf']):
            sb_amount_left  =   QSpinBox()
            sb_amount_left.setRange(0, 200)
            sb_amount_left.setSingleStep(1)
            sb_amount_left.setValue(this_item['left'])
            self.trw_menu_table.setItemWidget(wi_this_item, COLUMN_LEFT, sb_amount_left)

            sb_price        =   QSpinBox()
            sb_price.setRange(500, 2000000)
            sb_price.setSingleStep(500)
            sb_price.setSuffix(' VND')
            sb_price.setValue(this_item['price'])
            self.trw_menu_table.setItemWidget(wi_this_item, COLUMN_PRICE, sb_price)

        # add name and id, this goes for both leaf and branch
        le_item_name        =   QLineEdit()
        le_item_name.setMaxLength(15)
        le_item_name.setText(this_item['name'])

        wi_this_item.setText(COLUMN_ID, item_id)
        wi_this_item.setTextAlignment(COLUMN_ID, Qt.AlignCenter)

        self.trw_menu_table.setItemWidget(wi_this_item, COLUMN_ITEM_NAME, le_item_name)

    def remove_item(self, id_parent):
        this_item   =   self.trw_menu_table.currentItem()
        parent      =   this_item.parent()
        try:
            index   =   parent.indexOfChild(this_item)
            parent.takeChild(index)

            if (parent.childCount() == 0):
                # return to be a leaf, so we should give it its properties back
                sb_amount_left  =   QSpinBox()
                sb_amount_left.setRange(0, 200)
                sb_amount_left.setSingleStep(1)

                sb_price        =   QSpinBox()
                sb_price.setRange(500, 2000000)
                sb_price.setSingleStep(500)

                self.trw_menu_table.setItemWidget(parent, COLUMN_LEFT, sb_amount_left)
                self.trw_menu_table.setItemWidget(parent, COLUMN_PRICE, sb_price)

        except AttributeError:
            # item has no parents, aka top level item
            index   =   self.trw_menu_table.indexOfTopLevelItem(this_item)
            self.trw_menu_table.takeTopLevelItem(index)

    def toggle_selected_item(self, item, column):
        self.trw_menu_table.setCurrentItem(item, column, QItemSelectionModel.ToggleCurrent)

    def foo(self):
        item    =   self.trw_menu_table.findItems('2', Qt.MatchRecursive, COLUMN_ID)[0]

        name    =   self.trw_menu_table.itemWidget(item, COLUMN_ITEM_NAME).text()

        print(name)
