from .menu_update_view  import  View
from .menu_update_model import  Model
from PyQt5.QtWidgets    import  QApplication, QTreeWidgetItem
from PyQt5.QtCore       import  Qt

COLUMN_ITEM_NAME    =   0
COLUMN_ID           =   1
COLUMN_LEFT         =   2
COLUMN_PRICE        =   3

class Controller:
    def __init__(self):
        self.view       =   View()
        self.model      =   Model()

        self.view.bt_add.clicked.connect(self.add_new_item)
        self.view.bt_remove.clicked.connect(self.remove_item)
        self.view.bt_update.clicked.connect(self.update_menu)
        self.view.bt_cancel.clicked.connect(self.exit)

        self.view.trw_menu_table.itemClicked.connect(self.view.toggle_selected_item)
        self.view.trw_menu_table.itemChanged.connect(self.item_changed)

        self.model.load_menu()
        self.show_item_list()

        self.model.is_saved     =   True

        self.view.setModal(True)
        self.view.exec_()

    def add_new_item(self):
        self.view.add_item(id_item = self.model.current_id)
        self.model.current_id   += 1

    def remove_item(self):
        self.view.remove_item(id_parent = self.model.current_id)
        self.model.current_id   += 1

    def save_menu(self, this_item = None):
        if (this_item == None):
            #top level items
            top_item_amount = self.view.trw_menu_table.topLevelItemCount()

            for index in range(top_item_amount):
                # loop through every top level items
                this_item   =   self.view.trw_menu_table.topLevelItem(index)
                self.save_menu(this_item = this_item)
        else:
            self.view.trw_menu_table.setCurrentItem(this_item)
            is_leaf =   (this_item.childCount() == 0)
            parent  =   this_item.parent()

            if (parent == None):
                # top level leaf, has no parent
                parent_id   =   None
            else:
                parent_id   =   parent.text(COLUMN_ID)

            if (is_leaf):

                item_id     =   self.view.trw_menu_table.currentItem().text(COLUMN_ID)
                item_name   =   self.view.trw_menu_table.itemWidget(this_item, COLUMN_ITEM_NAME).text()
                item_left   =   self.view.trw_menu_table.itemWidget(this_item, COLUMN_LEFT).value()
                item_price  =   self.view.trw_menu_table.itemWidget(this_item, COLUMN_PRICE).value()
                
                leaf_data   =   dict( name      =   item_name,
                                      left      =   item_left,
                                      price     =   item_price,
                                      parent    =   parent_id,
                                      is_leaf   =   True)

                self.model.menu_items[item_id]  =   leaf_data
            else:
                branch_name =   self.view.trw_menu_table.itemWidget(this_item, COLUMN_ITEM_NAME).text()
                branch_id   =   self.view.trw_menu_table.currentItem().text(COLUMN_ID)

                branch_data =   dict( name      =   branch_name,
                                      parent    =   parent_id,
                                      is_leaf   =   False)

                self.model.menu_items[branch_id]  =   branch_data

                for index in range(this_item.childCount()):
                    next_item = this_item.child(index)
                    self.save_menu(this_item = next_item)

    def update_menu(self, checked, this_item = None):
        self.model.menu_items = {}
        self.save_menu()

        self.model.save_menu()
        self.model.send_mqtt()

    def item_changed(self):
        self.model.is_saved =   False

    def exit(self):

        if (not self.model.is_saved):
            from PyQt5.QtWidgets    import  QMessageBox
            confirm_quit =   QMessageBox.question(self.view, 'Thay đổi chưa được lưu', 'Có những thay đổi trong bảng chưa được lưu. Xác nhận huỷ?')

            if (confirm_quit    ==  QMessageBox.No):
                return

        self.view.close()

    def show_item_list(self):
        for item_id, item in self.model.menu_items.items():
            self.view.add_item(item_id = item_id, this_item = item)

        # ensure no ids will be duplicated
        self.model.current_id = len(self.model.menu_items)
