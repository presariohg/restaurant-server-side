import json
import codecs

class Model:

    menu_items  =   {}
    is_saved    =   True

    def __init__(self):
        self.current_id  =   0

    def load_menu(self):
        # with codecs.open('./data/menu.json', encoding = 'utf-8') as f:
        with codecs.open('./data/menu.json', encoding = 'utf-8') as f:
            self.menu_items = json.load(f)

        self.is_saved =   True

    def save_menu(self):
        with codecs.open('./data/menu.json', 'w', encoding = 'utf-8') as f:
            f.write(json.dumps(self.menu_items, ensure_ascii = False, indent = 2))

        self.is_saved =   True

    def send_mqtt(self):
        import paho.mqtt.publish    as  publish

        try:
            publish.single(topic = '/server/menu/', payload = json.dumps(self.menu_items, ensure_ascii = False), hostname = '127.0.0.1')
        except ConnectionRefusedError:
            print('Failed to send data. Have you started the mqtt server?')