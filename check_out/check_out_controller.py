from    .check_out_view     import  View
from    time                import  localtime, strftime

IS_DONE             =   2

class Controller:
    def __init__(self, table_order_list, menu, table_id):
        self.view       =   View(table_id)
        self.total      =   0
        self.order_list =   table_order_list
        self.table_id   =   table_id
        self.menu       =   menu

        self.is_check_out_accepted  =   False

        self.view.bb_confirm.accepted.connect(self.check_out_accept)

        self.show_receipt()
        self.view.show_total(self.total)

        self.view.setModal(True)
        self.view.exec_()

    def show_receipt(self):
        for order in self.order_list:
            if (order['status'] == IS_DONE):
                item_id     =   order['item_id']

                item_name   =   self.menu[item_id]['name']
                price       =   self.menu[item_id]['price']
                amount      =   order['amount']
                self.total  +=  int(price) * int(amount)

                self.view.add_item(item_name = item_name, price = price, amount = amount)


    def check_out_accept(self):
        self.is_check_out_accepted  =   True
        file_name   =   self.write_log()
        self.send_receipt(file_name)

    # well technically this function should go inside a model, but nah,
    # why bother make an entire new file just to store one function lol
    def write_log(self):
        file_name   =   strftime("%d.%m.%Y_%H.%M", localtime())

        total_cost  =   0
        table_id    =   self.table_id
        menu        =   self.menu
        order_list  =   self.order_list

        try:
            with open('./logs/Table {}/{}.log'.format(table_id + 1, file_name), 'w') as f:
                for item in order_list:
                    if (item['status'] ==  IS_DONE):
                        item_id     =   item['item_id']
                        item_name   =   menu[item_id]['name']
                        price       =   menu[item_id]['price']
                        amount      =   item['amount']
                        time        =   item['time']

                        cost        =   int(price) * int(amount)
                        total_cost  +=  cost

                        f.write('{} | {} {} x {} = {} VND\n'.format(time, amount, item_name, price, cost))
                    
                f.write('Tổng cộng: {} VND'.format(total_cost))
        except FileNotFoundError:
            import os

            os.system('mkdir -p ./logs/Table {}'.format(table_id + 1))
            self.write_log()

        return file_name

    def send_receipt(self, file_name):
        import paho.mqtt.publish    as  publish
        import codecs

        with codecs.open('./logs/Table {}/{}.log'.format(self.table_id + 1, file_name), encoding = 'utf-8') as log:
            try:
                publish.single(topic = '/T{}/receipt/'.format(self.table_id + 1), payload = log.read(), hostname = '127.0.0.1')
            except ConnectionRefusedError:
                print('Failed to send data. Have you started the mqtt server?')