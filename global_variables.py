#this file contains all constants

from PyQt5.QtGui        import  QColor

COLUMN_DISH      	=   0
COLUMN_AMOUNT       =   1
COLUMN_PRICE		=	2
COLUMN_TIME         =   2
COLUMN_TOTAL		=	3
COLUMN_STATUS       =   3
COLUMN_READY	    =   4

COLOR_NEW_ORDER     =   QColor("#7FC97F") 	# green-ish
COLOR_QUEUING_ORDER =   QColor("#F4E842") 	# orange-yellow-ish
COLOR_BLACK         =   QColor("#000000") 	# well, black ¯\_(ツ)_/¯
COLOR_WHITE			=	QColor("#FFFFFF")	# again, white ¯\_(ツ)_/¯
COLOR_DONE			=	QColor("#4B0082")	# indigo

TOPIC_NEW_ORDER     =   '/server/orders/'
TOPIC_WAITER        =   '/server/waiter/'
TOPIC_NEW_MENU      =   '/server/menu/'

is_beeping			=	False

IS_QUEUING          =   0
IS_DELIVERING       =   1
IS_DONE             =   2