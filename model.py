from    PyQt5.QtCore            import  QRunnable, pyqtSlot
from    time                    import  localtime, strftime, sleep
from    class_definitions       import  Table
from    global_variables        import  IS_DONE, IS_DELIVERING, IS_QUEUING, COLOR_BLACK, COLOR_DONE, COLOR_NEW_ORDER, TOPIC_NEW_MENU

import  paho.mqtt.client        as mqtt
import  re
import  os

import  json
import  codecs

class Thread_MQTT(QRunnable):
    def __init__(self, controller):
        super(Thread_MQTT, self).__init__()
        self.controller = controller

    @pyqtSlot()
    def run(self):
        self.controller.model.client.loop_forever()

class Thread_Beep(QRunnable):
    def __init__(self):
        super(Thread_Beep, self).__init__()

    @pyqtSlot()
    def run(self):
        from global_variables   import  is_beeping

        if (is_beeping):
            return
        else:
            is_beeping  =   True
            DURATION    =   0.3  # seconds
            os.system('play -nq -t alsa synth {} sine {}'.format(DURATION, 400))
            os.system('play -nq -t alsa synth {} sine {}'.format(DURATION, 500))
            os.system('play -nq -t alsa synth {} sine {}'.format(DURATION, 400))
            sleep(0.5)
            os.system('play -nq -t alsa synth {} sine {}'.format(DURATION, 400))
            os.system('play -nq -t alsa synth {} sine {}'.format(DURATION, 500))
            os.system('play -nq -t alsa synth {} sine {}'.format(DURATION, 400))
            is_beeping  =   False

class Model:
    def __init__(self):
        self.client                 =   mqtt.Client()
        self.table                  =   []

        for i in range(4):
            self.table.append(Table())

        self.table_to_change_color  =   -1
        self.color_to_change        =   COLOR_BLACK
        self.load_menu()

    def receive_waiter_report(self, message):
        try:
            table_id            =   int(re.findall(r'T(\d+?)#', message)[0]) - 1 #arrays start at 0
            # dish_list           =   re.findall(r'(\d+?)x\d+?', message)
            # amount_list         =   re.findall(r'\d+?x(\d+?)', message)
            # time                =   strftime("%H:%M:%S ║ %d/%m/%Y",gmtime())

            self.table_to_change_color  =   table_id
            self.color_to_change        =   COLOR_DONE

            for order in self.table[table_id].order:

                if (order['status']    ==  IS_DELIVERING):
                    order['status']     =   IS_DONE

                    # never send more than what's left in storage
                    item_id             =   order['item_id']
                    order['amount']     =   min(self.menu_items[item_id]['left'], order['amount'])

                    # notify table that it was received these items
                    received_item       =   {'item_id'  :   order['item_id'],
                                             'amount'   :   order['amount']}

                    self.client.publish('/T{}/'.format(table_id + 1), json.dumps(received_item, ensure_ascii = False))

        except IndexError:
            print('Faulty message syntax')

    def receive_order(self, message):
        try:
            id_table            =   int(re.findall(r'T(\d+?)#', message)[0]) - 1 #arrays start at 0
            id_items            =   re.findall(r'#(\d+?)x\d+?', message)
            amount_list         =   re.findall(r'#\d+?x(\d+)', message)
            time                =   strftime("%H:%M", localtime())

            number_of_orders    =   len(id_items)

            self.table_to_change_color  =   id_table
            self.color_to_change        =   COLOR_NEW_ORDER

            for i in range(number_of_orders):
                this_order_data =   { 'item_id' :   id_items[i],
                                      'amount'  :   amount_list[i],
                                      'time'    :   time,
                                      'status'  :   IS_QUEUING,
                                      'is_ready':   False,}

                self.table[id_table].order.append(this_order_data)

        except IndexError:
            print('Faulty message syntax')
    
    def deliver(self):

        table_id            =   0;
        for table in (self.table):
            table_id        +=  1
            mqtt_str        =   'T{}'.format(table_id)
            is_send_mqtt    =   False
            for order in (table.order):
                if ((order['is_ready']) and (order['status'] == IS_QUEUING)):
                    is_send_mqtt    =   True

                    item_id         =   order['item_id']

                    # never send more than amount left in storage
                    order['amount'] =   min(self.menu_items[item_id]['left'], int(order['amount']))

                    mqtt_str        +=  '#{}x{}'.format(order['item_id'], order['amount'])
                    order['status'] =   IS_DELIVERING

                    # reduce number of availablte items in storage
                    self.reduce_left_item(order)

            if (is_send_mqtt):
                self.client.publish("/waiter/", mqtt_str)

                # update new menu info since left items have just been changed
                self.save_menu()

                # send the new menu data over mqtt to tables
                self.client.publish(TOPIC_NEW_MENU, json.dumps(self.menu_items, ensure_ascii = False))

        self.client.publish('/waiter/', "GO")

    def reduce_left_item(self, order):

        item_id     =   order['item_id']
        amount_sent =   order['amount']

        self.menu_items[item_id]['left'] -= int(amount_sent)

    def save_menu(self):
        with codecs.open('./data/menu.json', 'w', encoding = 'utf-8') as menu:
            menu.write(json.dumps(self.menu_items, ensure_ascii = False, indent = 2))

    def load_menu(self):
        with codecs.open('./data/menu.json', encoding = 'utf-8') as menu:
            self.menu_items = json.load(menu)
